public class AccountTriggerHandler extends TriggerHandler{
    
    public AccountTriggerHandler() {
    }
    
    public override void afterUpdate() {
        
      Set<ID> accountIds = new Set<ID>();
        for(Account acc : (List<Account>)Trigger.new){
            if(acc.External_Status__c != ((Map<Id, Account>)Trigger.oldMap).get(acc.Id).External_Status__c){
                if(!accountIds.contains(acc.Id)) accountIds.add(acc.Id);
            }
        }
      
      List<Contact> lstContacts = [SELECT Id, Name, External_Status__c, AccountId FROM Contact WHERE AccountId = :accountIds];
      Map<Id, List<Contact>> mapContacts = new Map<Id, List<Contact>>();
        
        for(Contact con : lstContacts){
            
            if(!mapContacts.containsKey(con.AccountId))
                mapContacts.put(con.AccountId, new List<Contact>());
            
            mapContacts.get(con.AccountId).add(con);
            
        }
        
        List<Contact> lstCon = new List<Contact>();
        
        for(Account acc : (List<Account>)Trigger.new){
            
            if(mapContacts.containsKey(acc.Id)){ 
                List<Contact> lstAccContacts = mapContacts.get(acc.Id);
                
                if(!lstAccContacts.isEmpty()){
                    for(Contact ctt : lstAccContacts){
                        if(ctt.External_Status__c != 'Inactive'){
                            ctt.External_Status__c = acc.External_Status__c;
                            lstCon.add(ctt);
                        }
                    }        
                }
            }
            
        }
        
        update lstCon;
     
    }  
    
}