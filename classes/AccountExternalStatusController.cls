public class AccountExternalStatusController {
    
    Account account;

    public Account getAccount() {
        if(account == null)
        account = [SELECT Id, Name, External_Status__c from Account WHERE id = :ApexPages.currentPage().getParameters().get('id')]; 
        return account;
    } 
    
    public PageReference save(){
        
        update account;
        
        PageReference accountPage = new ApexPages.StandardController(account).view();
        accountPage.setRedirect(true);
        return accountPage;
    }
     
}