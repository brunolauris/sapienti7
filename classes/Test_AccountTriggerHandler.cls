@isTest
public class Test_AccountTriggerHandler {
    
	 static testMethod void ExternalStatusTest() 
	 {
		Account testAccount = new Account();
		testAccount.Name='Test Account' ;
        testAccount.External_Status__c = 'Actived';
		insert testAccount;

		Contact cont = new Contact ();
		cont.FirstName = 'FirstName';
		cont.LastName = 'LastName';
		cont.Email='email@email.com';
		cont.phone='12345678';
		insert cont;
		
		Test.StartTest(); 

        testAccount.External_Status__c = 'Suspended';
        update testAccount;

		Test.StopTest();
	 }

}